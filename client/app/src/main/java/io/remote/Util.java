package io.remote;

import android.app.AlarmManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




/**
 * utility methods for various stuff and things
 */
public class Util {


    private static String TAG = "shc.pipeline.Util";

    public static String wrapInQuotes(String datestr)
    {
        return String.format ("\"%s\"", datestr);
    }



    public static String readStreamIntoString(InputStream in) throws IOException
    {
        BufferedReader bufreader = new BufferedReader(new InputStreamReader(in));
        String inputline;
        StringBuffer result_buffer = new StringBuffer();

        while ( (inputline = bufreader.readLine()) != null)
        {
            result_buffer.append(inputline);
        }

        bufreader.close();
        return result_buffer.toString();
    }

    public static String urlSegment(Object[] components)
    {
        StringBuilder sb = new StringBuilder();

        for (int i=0; i<components.length; ++i)
        {
            sb.append(components[i]);
            if (i < components.length - 1)
                sb.append('/');

        }
        return sb.toString();
    }

    public static <T> List<T> emptyList()
    {
        return emptyList(0);
    }
    public static <T> List<T> emptyList(int n)
    {
        return new ArrayList<>(n);
    }

    public static <K, V> Map<K,V> emptyHashMap()
    {
        return new HashMap<K, V>();
    }



    public static <V extends View>
    V findChildView(View v, int id) {
        return (V) v.findViewById(id);
    }


    public static String stringFromBytes(byte[] bytes)
    {
        return stringFromBytes(bytes, "UTF-8");
    }

    public static String stringFromBytes(byte[] bytes, String encoding)
    {
        String result = "";
        try
        {
            result = new String(bytes, encoding);
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static boolean checkBits(int fields, int flagbits)
    {
        return (fields & flagbits) == flagbits;
    }

    static String printStackTrace(Throwable throwable) {
        String message = throwable.getMessage();
        StackTraceElement[] trace = throwable.getStackTrace();
        StringBuilder builder = new StringBuilder(message + ":\n");

        for (StackTraceElement item : trace) {
            builder.append(item.toString());
            builder.append('\n');
        }

        return builder.toString();
    }

    public static final class Sys {

        public static <ServiceType>  ServiceType service(String service_name)
        {
            //return (ServiceType) Pipeline.get().getSystemService(service_name);
            return null;
        }

        public static LayoutInflater getLayoutInflater()
        {
            return service(Context.LAYOUT_INFLATER_SERVICE);
        }

        public static AlarmManager getAlarmService()
        {
            return service(Context.ALARM_SERVICE);
        }

        public static ConnectivityManager getConnectivityManager()
        {
            return service(Context.CONNECTIVITY_SERVICE);
        }
    }
}
