package io.remote;

/**
 * static configuration vars
 */

public class Config {
    public static String HOST_URI = "tcp://192.168.1.2:1883";
    public static final int MQTT_CONN_TIMEOUT = 3;
}
