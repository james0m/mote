package io.remote;

import android.util.Log;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 *
 */
public class HostConnection implements MqttCallbackExtended {

    public static final String TAG = "HostConnection";

    public interface ReadyListener {
        void ready();
    }

    String mClientId;
    String mHostUri;
    MqttAsyncClient mClient;

    ReadyListener mReadyListener;

    public HostConnection(String url) {
        mClientId = MqttClient.generateClientId();
        mHostUri = url;
    }

    public boolean isConnected() {
        return mClient != null && mClient.isConnected();
    }

    public void connect()
    {
        Log.i(TAG, "attempting to connect to host: " + mHostUri);
        MqttConnectOptions opts = new MqttConnectOptions();
        opts.setCleanSession(true);
        opts.setConnectionTimeout(Config.MQTT_CONN_TIMEOUT);
        create_mqtt_client();

        try {
            mClient.connect(opts, null, new IMqttActionListener() {
                @Override public void onSuccess(IMqttToken asyncActionToken) {
                    Log.d(TAG, String.format("connection to host %s was established", mHostUri));
                    if(mReadyListener != null)
                        mReadyListener.ready();
                }
                @Override public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.d(TAG, String.format("connection to host %s could not be established",
                            mHostUri), asyncActionToken.getException());
                }
            });
        }
        catch (MqttException me) {

            String msg = String.format("error trying to connect to host: %s",
                    me.getMessage());

            Log.e(TAG, msg);
            mHostUri = null;

        }
    }

    public void disconnect() {
        try {
            mClient.disconnect();
        }
        catch (MqttException me) {
            Log.d(TAG, "error during disconnect: " + me.getMessage());
        }
    }

    private void create_mqtt_client() {
        try {
            mClient = new MqttAsyncClient(mHostUri, mClientId, new MemoryPersistence());
        }
        catch (MqttException error) {
            Log.e(TAG, "error while creating MqttAsyncClient: " + error.getMessage());
            mClient = null;
        }
    }
    /**
     * send the remote control signal to the host
     */
    public void broadcast(String topic, String message) {
        MqttMessage msg = new MqttMessage();

        msg.setQos(1);
        msg.setPayload(message.getBytes());

        try {
            mClient.publish(topic, msg);
            Log.i(TAG, "broadcasted msg to host");
        }
        catch (MqttException me) {
            String logmsg = String.format("error trying to publish message: %s",
                    me.getMessage());
            Log.e(TAG, logmsg);
        }
    }

    public void setReadyListener(ReadyListener listener) {
        mReadyListener = listener;
    }

    @Override //MqttCallbackExtended
    public void connectComplete(boolean reconnect, String serverURI) {
        Log.d(TAG, String.format("connectComplete: reconnect %s; serverURI: %s", reconnect, serverURI));
        if(mReadyListener != null)
            mReadyListener.ready();
    }

    @Override //MqttCallbackExtended
    public void connectionLost(Throwable cause) {
        Log.d(TAG, "connection lost", cause);
    }

    @Override //MqttCallbackExtended
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        Log.i(TAG, String.format("messageArrived: topic: %s; message: %s", topic, message.toString()));
    }

    @Override //MqttCallbackExtended
    public void deliveryComplete(IMqttDeliveryToken token) {
        Log.i(TAG, String.format("message delivery complete"));
    }
}
