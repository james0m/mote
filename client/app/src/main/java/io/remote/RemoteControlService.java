package io.remote;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;


import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import static android.content.Intent.EXTRA_TEXT;

/**
 *
 */
public class RemoteControlService extends IntentService implements HostConnection.ReadyListener {

    public static final String TAG = "RemoteControlService";
    public static final String EXTRA_URL = "io.remote.extra.URL";

    HostConnection conn;

    List<Intent> mIntentQueue = Util.emptyList();

    public RemoteControlService()
    {
        super("RemoteControlService");
        createHostConnection();
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Events.register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "shutting down");
    }

    public static void sendYoutubeVideo(Context context, String url) {
        Intent intent = new Intent(context, RemoteControlService.class);
        intent.setAction(Actions.SEND_YOUTUBE_VIDEO);

        intent.putExtra(EXTRA_URL, url);
        context.startService(intent);
    }

    private void createHostConnection() {

        String host_ip = Remote.getAppPreferences().getString("host_ip", null);

        if(host_ip != null) {
            conn = new HostConnection(String.format("tcp://%s:1883",host_ip));
            conn.setReadyListener(this);
            conn.connect();
        }
        else {
            Log.d(TAG, "createHostConnection(): no host ip was found");
        }
    }
    @Subscribe
    public void onHostUriChanged(Events.HostUriChanged event) {
        conn.disconnect();
        createHostConnection();
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        Log.d(TAG, "onHandleIntent");
        if (intent != null) {
           final String action = intent.getAction();

           if (Intent.ACTION_SEND.equalsIgnoreCase(action)) {

               if( conn.isConnected()) {
                   processSendIntent(intent);
               }
               else
                   queueIntent(intent);

           }
           else {
               Log.d(TAG, String.format("onHandleIntent: received intent with unknown action %s",
                       action));
           }
        }
        else {
            Log.d(TAG, "sent null intent");
        }
    }

    private void queueIntent(Intent intent) {
        Log.i(TAG, "adding intent to queue for later processing");
        mIntentQueue.add(intent);
    }

    private void processSendIntent(Intent intent) {
        String raw_url = intent.getStringExtra(EXTRA_TEXT);
        String msg = "Received ACTION_SEND. url: " + raw_url;

        do_send_youtube_video(raw_url);
        Log.d(TAG, msg);
    }

    /**
     * package up the url into mqtt message on the topic youtube, send to connected host
     * @param url
     */
    private void do_send_youtube_video(String url) {
        conn.broadcast("remote/signal/youtube", url);
    }

    public int onStartCommand(Intent intent, int flags, int startId)
    {
        super.onStartCommand(intent, flags, startId);
        Toast.makeText(this.getApplicationContext(), TAG + " service starting...", Toast.LENGTH_SHORT)
                .show();
        return START_STICKY;
    }

    @Override
    public void ready()
    {
        Log.d(TAG, "host connection is ready");
        if (mIntentQueue.size() > 0 ){
            for(Intent i : mIntentQueue)
                processSendIntent(i);

            mIntentQueue.clear();
        }
    }

}
