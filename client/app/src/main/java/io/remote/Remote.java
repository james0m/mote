package io.remote;


import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 *
 */
public class Remote extends Application {

    private static Remote self;

    @Override
    public void onCreate() {
        super.onCreate();
        self = this;
    }

    public Remote get() {
        return self;
    }

    public static SharedPreferences getAppPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(self.getApplicationContext());
    }
}
