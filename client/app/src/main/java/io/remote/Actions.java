package io.remote;

import android.content.Intent;

/**
 *
 */

public class Actions {

    public static final String SEND_WEBSITE_URL = "io.remote.actions.SENDWEBSITEURL";
    public static final String SEND_YOUTUBE_VIDEO = "io.remote.actions.SENDYOUTUBEVIDEO";

}
