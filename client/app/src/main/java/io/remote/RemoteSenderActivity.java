package io.remote;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


/**
 * this activity is needed for catching the ACTION_SEND intents
 * and starting the remote service
 */
public class RemoteSenderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_sender);

        Intent intent = getIntent();
        intent.setClass(getApplicationContext(), RemoteControlService.class);
        startService(intent);


        finish();
    }
}
