package io.remote;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by james on 12/9/16.
 */

public class Events {

    private static final EventBus s_eventbus = EventBus.builder()
            .logNoSubscriberMessages(true)
            .build();

    public static void register(Object listener) {
        s_eventbus.register(listener);
    }

    public static void post(Object event) {
        s_eventbus.post(event);
    }

    public static class HostUriChanged {

    }
}
