

import paho.mqtt.client as mqtt
from subprocess import call as run

import config


hostname = "lightningbox"

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, rc):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.publish("remote/host/status/%s" % hostname, "online", 1, True)
    
    for topic in config.TOPICS:
        client.subscribe("remote/signal/" + topic)

def on_disconnect(client, userdata, rc):
    print ("Disconnected with result code " + str(rc))
    client.publish("remote/host/status/%s" % hostname, "offline", 1, True)
    
# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))
    command = [ config.URL_HANDLER, msg.payload ]
    run(command)

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.on_disconnect = on_disconnect

client.will_set( topic="remote/host/status/%s" % hostname,
                 payload="offline",
                 qos=1,
                 retain=True)

client.connect(config.HOST_URI,
               config.HOST_PORT,
               config.HOST_KEEPALIVE_INTERVAL)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
