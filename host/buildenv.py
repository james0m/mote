from subprocess import call as run


run(["virtualenv", "env"]);

command = ["env/bin/pip", "install"]


deps = [
    "paho-mqtt"
]


run(command + deps)
