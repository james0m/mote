
# connection setup
HOST_URI                = "192.168.1.2"
HOST_PORT               = 1883
HOST_KEEPALIVE_INTERVAL = 60



# mqtt topics to listen in on 
TOPICS = [ "youtube", "netflix" ]

# the browser command used to handle uris
URL_HANDLER             = "firefox"


